# Django Container Stack

This project was built for me to practice and learn python as well as the Django framework. I am aiming to containerize a Django app along with Postgres and make a simple admin + client side application. Mostly from following the [Django tutorial](https://docs.djangoproject.com/en/1.11/intro/tutorial01/). Other goals are running it through Travis CI. If CI is successful I am building the container image and pushing it to dockerhub.

## Developers Notes

I have made a Makefile for a more automated development process

If you need to build the project

```bash
make build
```

If you need to stand up the django stack you can run

```bash
make up
```

## Requirements

### For pulling and running the container only

1. Docker
2. Docker Compose
3. git

### For local development
1. python3
2. pip
3. django
4. virtualenv

