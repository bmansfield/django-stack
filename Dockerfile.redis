FROM centos:centos7

LABEL maintainer="Byron Mansfield <byron@byronmansfield.com>" \
    Vendor="CentOS" \
    License="GPLv2" \
    Name="Redis Image"

ARG BUILD_DATE

#Set environment vars
ENV EL_VERSION="7" \
    BUILD_DATE=${BUILD_DATE} \
    LANG="en_US.UTF-8" \
    LANGUAGE="en_US:en" \
    LC_ALL="en_US.UTF-8"

#install basics
RUN localedef -c -i en_US -f UTF-8 en_US.UTF-8 && \
    yum install --setopt=tsflags=nodocs -y centos-release-scl \
        https://dl.fedoraproject.org/pub/epel/epel-release-latest-${EL_VERSION}.noarch.rpm \
        https://centos7.iuscommunity.org/ius-release.rpm && \
    yum install --setopt=tsflags=nodocs -y \
        redis && \
    \
    # clean up
    yum --setopt=tsflags=nodocs -y update && \
    rm -rf /var/cache/yum && \
    yum clean all

EXPOSE 6379

ENTRYPOINT /usr/bin/redis-server
